#!/bin/bash
#
# Copyright IBM Corp. All Rights Reserved.
#
# SPDX-License-Identifier: Apache-2.0
#

# import utils
#set -e
eval "$(cat network-deploy.sh | head -10)"
. scripts/configUpdate.sh

# NOTE: this must be run in a CLI container since it requires jq and configtxlator 
createAnchorPeerUpdate() {
  infoln "Fetching channel config for channel $CHANNEL_NAME"
#  fetchChannelConfig $ORG $CHANNEL_NAME ${CORE_PEER_LOCALMSPID}config.json
  fetchChannelConfig
  infoln "Generating anchor peer update transaction for Org${ORG} on private environment $CHANNEL_NAME"
  which jq
  if [ "$?" -ne 0 ]; then
    if [ "`whoami`" == 'root' ]; then
      apt-get install -y jq
    else
      sudo apt-get install -y jq
    fi
    which jq
  fi
  set -x
  # Modify the configuration to append the anchor peer 
  jq '.channel_group.groups.Application.groups.'${ORG_MSP}'.values += {"AnchorPeers":{"mod_policy": "Admins","value":{"anchor_peers": [{"host": "'$HOST'","port": '$PORT'}]},"version": "0"}}' ${ORG_MSP}config.json > ${ORG_MSP}modified_config.json
  { set +x; } 2>/dev/null

  # Compute a config update, based on the differences between 
  # {orgmsp}config.json and {orgmsp}modified_config.json, write
  # it as a transaction to {orgmsp}anchors.tx
  createConfigUpdate ${CHANNEL_NAME} ${ORG_MSP}config.json ${ORG_MSP}modified_config.json # ${ORG_MSP}anchors.tx
}

updateAnchorPeer() {
  kubectl cp ${ORG_MSP}anchors.tx $HOST:/opt/gopath/src/github.com/hyperledger/fabric/peer/${ORG_MSP}anchors.tx
  kubectl exec $HOST -- peer channel update -o $ORDERER_NAME:$ORDERER_PORT --ordererTLSHostnameOverride $ORDERER_NAME -c $CHANNEL_NAME -f ${ORG_MSP}anchors.tx --tls --cafile "$ORDERER_CA_DIR" >&log.txt
  res=$?
  cat log.txt
  verifyResult $res "Anchor peer update failed"
  temp_msp=`echo ${ORG_MSP%???} | cut -f2 -d"g"`
  successln "Anchor peer set for organization ${temp_msp} on private environment '$CHANNEL_NAME'"
}

#ORG=$1
#CHANNEL_NAME=$2

HOST=$1
PORT=$2
ORG_MSP=$3
CHANNEL_NAME=$4

#setGlobalsCLI $ORG

createAnchorPeerUpdate 

updateAnchorPeer 

mv *.pb *.tx *config*.json $DLT_CONFIG_DIR
