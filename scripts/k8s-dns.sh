#!/bin/bash

eval "$(cat network-deploy.sh | head -10)"

if [ ! $1 ]; then
	println "Usage: $0 \033[0;32m<pledger-dlt-p-01, pledger-dlt-o-0, all>\033[0m"
	exit 0
fi
INPUT=$1

TEMP="k8s-dns-records"
kubectl get po -o wide --no-headers | awk '{print $6, "\t", $1}' | grep "pledger-dlt" > $TEMP
pods=( `cat $TEMP | awk '{print $2}'| tr '\n' ' '` )
# kubectl get svc | grep svc | awk '{print $3, "\t", $1}' >> $TEMP

if [ $INPUT == 'all' ]; then 

	infoln "Arranging DNS ..."
	for pod in "${pods[@]}"
	do
		kubectl cp $TEMP $pod:/
		kubectl exec $pod -- sh -c "cat /"$TEMP" >> /etc/hosts" # IS PERMANENT
	done

	# k get svc -o wide --no-headers -l svc=p-01 
else
	NEW_IP=`kubectl get po -o wide | awk '{if ($1 =="'${INPUT}'") print $6}'`
	if [ ! $NEW_IP ]; then
		println "\033[0;31mInput resource must be already up! Aborting...\033[0m"
		exit 0
	fi

	infoln "Arranging DNS ..."
	for pod in "${pods[@]}"
	do
		kubectl exec $pod -- sh -c "echo "$NEW_IP" "$INPUT" >> /etc/hosts" # IS PERMANENT
		# kubectl exec $pod -- sh -c "sed -i '/"$INPUT"/s/.*./"$NEW_IP" \t "$INPUT"/' /etc/hosts" # :Resource busy
	done
	
	kubectl cp $DEPLOYMENT_DIR/$TEMP $INPUT:/
	kubectl exec $INPUT -- sh -c "cat /"$TEMP" >> /etc/hosts" # IS PERMANENT

fi

mv $TEMP $DEPLOYMENT_DIR
