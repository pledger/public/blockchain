#!/bin/bash
set -e
ARTIFACTORY_DOCKER_REGISTRY=$1

export dltConfigDir="dlt" # 
export ccBuildersDir="cc-builders"
eval "$(cat scripts/envVar.sh | grep IMAGETAG)"
export IMAGETAG CA_IMAGETAG

eval "$(cat network-deploy.sh | head -10 | grep DLT_CONFIG_DIR)"
cp $DLT_CONFIG_DIR/system-genesis-block/.gitkeep $DLT_CONFIG_DIR/channel-artifacts/
BUILD_IMG_YAML='manifests/build-images.yaml'
docker-compose -f $BUILD_IMG_YAML build

source scripts/utils.sh
images=( `cat $BUILD_IMG_YAML | grep image: | cut -f2 -d"m" | awk '{print $2}' | tr '\n' ' '` )

if [ "`kubectl config current-context | grep microk8s | wc -l`" -eq 1 ]; then
  for img in "${images[@]}"
  do
    infoln "Importing $img in microk8s registry..."
    docker save $img > compressed.tar
    microk8s ctr image import compressed.tar
    microk8s ctr images ls | grep $img
  done
  rm compressed.tar
else
  if [ ! $ARTIFACTORY_DOCKER_REGISTRY ]; then
    println "Usage: $0 \033[0;32m<IP ADDRESS>/registryname\033[0m, for remote private docker registry."
    exit 0
  fi
  for img in "${images[@]}"
  do
    docker tag $img $ARTIFACTORY_DOCKER_REGISTRY$img
    docker images | grep $img
    infoln "Pushing $img in registry..."
    docker push $ARTIFACTORY_DOCKER_REGISTRY$img
    docker images | grep $img
    docker rmi $ARTIFACTORY_DOCKER_REGISTRY$img $img
  done
  docker image prune -f
fi
