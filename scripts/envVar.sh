#!/bin/bash
# TODO bring here the images names (e.g. pledger-dlt-p-01,..) and put ARG=$PEER0_ORG1 in manifests/Dockerfile
. scripts/utils.sh

# Using crpto vs CA. default is cryptogen
CRYPTO="cryptogen"
# timeout duration - the duration the CLI should wait for a response from
# another container before giving up
MAX_RETRY=5
# default for delay between commands
CLI_DELAY=3
DELAY=3
VERBOSE="false"
# channel name defaults to "mychannel"
CHANNEL_NAME="mychannel"
# chaincode name defaults to "NA"
CC_NAME="NA"
# chaincode path defaults to "NA"
CC_SRC_PATH="NA"
# endorsement policy defaults to "NA". This would allow chaincodes to use the majority default policy.
CC_END_POLICY="NA"
# collection configuration defaults to "NA"
CC_COLL_CONFIG="NA"
# chaincode init function defaults to "NA"
CC_INIT_FCN="NA"
# use this as the default docker-compose yaml definition
COMPOSE_FILE_ALL=manifests/docker-compose-all.yaml
# Base stack name
STACK_NAME="FABRIC_STACK"
# use this as the default docker-compose yaml definition
COMPOSE_FILE_BASE=manifests/test-network.yaml
export COMPOSE_FILE_BASE="manifests/test-network.yaml"
PEER_NEW_FILE=manifests/peer-new.yaml
BLO_EXPLO_DB=manifests/explorer-db.yaml
BLO_EXPLO=manifests/explorer.yaml
# docker-compose.yaml file if you are using couchdb
COMPOSE_FILE_COUCH=manifests/docker-compose-couch.yaml
# certificate authorities compose file
COMPOSE_FILE_CA=manifests/ca.yaml
# NSF server and abstract volume
VOL=manifests/volumes.yaml
# use this as the docker compose couch file for org3
COMPOSE_FILE_COUCH_ORG3=addOrg3/docker/docker-compose-couch-org3.yaml
# use this as the default docker-compose yaml definition for org3
COMPOSE_FILE_ORG3=addOrg3/docker/docker-compose-org3.yaml
#
# use go as the default language for chaincode
CC_SRC_LANGUAGE="go"
# Chaincode version
CC_VERSION="1.0"
# Chaincode definition sequence
CC_SEQUENCE=1
# default image tag
IMAGETAG="2.3.2"
# default ca image tag
CA_IMAGETAG="1.5.2"
# ca credentials
CA_USR="admin"
CA_PSW="adminpw"
# default database
DATABASE="leveldb"
# overlay Swarm network name
NETWORK="test"
# Consortium profile from configtx/configtx.yaml
CONSORTIUM_PROFILE="TwoOrgsApplicationGenesis"
# Channel profile from configtx/configtx.yaml
CHANNEL_PROFILE="OrgsChannel"
# Nodes
N1="odokom"
N2="odokom"
# Genesis Block
GENESIS_BLOCK="genesis.block" # $DLT_CONFIG_DIR/system-genesis-block/genesis.block
IMAGES_DOCKERFILE="manifests/Dockerfile"



# fetch pod names with parse_yaml
. scripts/parse-yaml.sh

CA_ORG1_NAME=$(cat $IMAGES_DOCKERFILE | grep CA_NAME | awk '{print $3}' | cut -d'=' -f2 | sed '1q;d')
CA_ORG1_PORT=$(cat $IMAGES_DOCKERFILE | grep SERVER_PORT | awk '{print $2}' | cut -d'=' -f2 | sed '1q;d')
CA_ORG2_NAME=$(cat $IMAGES_DOCKERFILE | grep CA_NAME | awk '{print $3}' | cut -d'=' -f2 | sed '2q;d')
CA_ORG2_PORT=$(cat $IMAGES_DOCKERFILE | grep SERVER_PORT | awk '{print $2}' | cut -d'=' -f2 | sed '2q;d')
CA_ORDERER_NAME=$(cat $IMAGES_DOCKERFILE | grep CA_NAME | awk '{print $3}' | cut -d'=' -f2 | sed '3q;d')
CA_ORDERER_PORT=$(cat $IMAGES_DOCKERFILE | grep SERVER_PORT | awk '{print $2}' | cut -d'=' -f2 | sed '3q;d')
#export CA_ORG1_IP=$(kubectl get pod $CA_ORG1_NAME -o wide --no-headers | awk '{print $6}')
#export CA_ORG2_IP=$(kubectl get pod $CA_ORG2_NAME -o wide --no-headers | awk '{print $6}')
#export CA_ORDERER_IP=$(kubectl get pod $CA_ORDERER_NAME -o wide --no-headers | awk '{print $6}')
ORDERER_NAME=$(cat $IMAGES_DOCKERFILE | grep hyperledger/fabric-orderer | awk '{print $4}')
#ORDERER_SVC=$(parse_yaml $COMPOSE_FILE_BASE | grep metadata_name=\"  | awk -F'"' '{print $2}' | sed '4q;d')
ORDERER_PORT=$(cat $IMAGES_DOCKERFILE | grep ORDERER_GENERAL_LISTENPORT  | awk '{print $1}' | cut -d '=' -f2)
ORDERER_ADMINPORT=$(cat $IMAGES_DOCKERFILE | grep ORDERER_ADMIN_LISTENADDRESS_PORT  | awk '{print $1}' | cut -d '=' -f2)
PEER0_ORG1=$(cat $IMAGES_DOCKERFILE | grep CORE_PEER_ID  | awk '{print $1}' | cut -d '=' -f2 | sed '1q;d')
PEER0_ORG1_PORT=$(cat $IMAGES_DOCKERFILE | grep CORE_PEER_ADDRESS  | awk '{print $1}' | cut -d ':' -f2 | sed '1q;d')
PEER0_ORG1_CCPORT=$(cat $IMAGES_DOCKERFILE | grep CORE_PEER_CHAINCODEADDRESS  | awk '{print $1}' | cut -d ':' -f2 | sed '1q;d')
PEER0_ORG2=$(cat $IMAGES_DOCKERFILE | grep CORE_PEER_ID  | awk '{print $1}' | cut -d '=' -f2 | sed '2q;d')
PEER0_ORG2_PORT=$(cat $IMAGES_DOCKERFILE | grep CORE_PEER_ADDRESS  | awk '{print $1}' | cut -d ':' -f2 | sed '2q;d')
PEER0_ORG2_CCPORT=$(cat $IMAGES_DOCKERFILE | grep CORE_PEER_CHAINCODEADDRESS  | awk '{print $1}' | cut -d ':' -f2 | sed '2q;d')
EXPLORER_DB=$(cat $IMAGES_DOCKERFILE | grep hyperledger/explorer-db | awk '{print $4}')
EXPLORER_DB_PORT=$(cat $IMAGES_DOCKERFILE | grep XDB_PORT | awk '{print $1}' | cut -d '=' -f2)
EXPLORER=$(cat $IMAGES_DOCKERFILE | grep hyperledger/explorer: | awk '{print $4}')
EXPLORER_PORT=$(cat $IMAGES_DOCKERFILE | grep XPL_PORT | awk '{print $1}' | cut -d '=' -f2)
#EXPLORER_SVC=$(parse_yaml $BLO_EXPLO | grep metadata_name=\"  | awk -F'"' '{print $2}' | sed '2q;d')

export CA_ORG1_NAME CA_ORG1_PORT CA_ORG2_NAME CA_ORG2_PORT CA_ORDERER_NAME CA_ORDERER_PORT ORDERER_ADMINPORT
export ORDERER_NAME ORDERER_PORT PEER0_ORG1 PEER0_ORG1_PORT PEER0_ORG1_CCPORT PEER0_ORG2 PEER0_ORG2_PORT PEER0_ORG2_CCPORT
export EXPLORER EXPLORER_PORT EXPLORER_DB EXPLORER_DB_PORT

export CORE_PEER_TLS_ENABLED=true
#export ORDERER_CA_DIR=ordererOrganizations/example.com/orderers/orderer-example-com/msp/tlscacerts/tlsca.example.com-cert.pem
#export ORDERER_CA_DIR=/var/hyperledger/orderer/tlsca.example.com-cert.pem
#export ORDERER_CA_DIR=/etc/hyperledger/fabric/ordererOrganizations/example.com/orderers/orderer-example-com/msp/tlscacerts/tlsca.example.com-cert.pem
export ORDERER_CA_DIR=/etc/hyperledger/fabric/ordererOrganizations/example.com/msp/tlscacerts/tlsca.example.com-cert.pem
export PEER0_ORG1_CA=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0-org1-example-com/tls/ca.crt
export PEER0_ORG2_CA=${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0-org2-example-com/tls/ca.crt
export PEER0_ORG3_CA=${PWD}/organizations/peerOrganizations/org3.example.com/peers/peer0-org3-example-com/tls/ca.crt

# Set OrdererOrg.Admin globals
export ORDERER_MSP=`cat $IMAGES_DOCKERFILE | grep ORDERER_GENERAL_LOCALMSPID= | awk '{print $1}' | cut -d '=' -f2`
export ORDERER_POL_R="OR('"$ORDERER_MSP".member')"
export ORDERER_POL_W="OR('"$ORDERER_MSP".member')"
export ORDERER_POL_A="OR('"$ORDERER_MSP".admin')"
export ORG1_MSP=`cat $IMAGES_DOCKERFILE | grep CORE_PEER_LOCALMSPID= | awk '{print $1}' | cut -d '=' -f2 | sed '1q;d'`
export ORG2_MSP=`cat $IMAGES_DOCKERFILE | grep CORE_PEER_LOCALMSPID= | awk '{print $1}' | cut -d '=' -f2 | sed '2q;d'`

export ORDERER_CA=${PWD}/organizations/ordererOrganizations/example.com/orderers/orderer.example.com/msp/tlscacerts/tlsca.example.com-cert.pem
export PEER0_ORG1_CA=${PWD}/organizations/peerOrganizations/org1.example.com/peers/peer0-org1-example-com/tls/ca.crt
export PEER0_ORG2_CA=${PWD}/organizations/peerOrganizations/org2.example.com/peers/peer0-org2-example-com/tls/ca.crt
export PEER0_ORG3_CA=${PWD}/organizations/peerOrganizations/org3.example.com/peers/peer0.org3.example.com/tls/ca.crt
export ORDERER_ADMIN_TLS_SIGN_CERT=/var/hyperledger/orderer/tls/server.crt # ordererOrganizations/example.com/orderers/orderer.example.com/tls/server.crt
export ORDERER_ADMIN_TLS_PRIVATE_KEY=/var/hyperledger/orderer/tls/server.key # ordererOrganizations/example.com/orderers/orderer.example.com/tls/server.key

# Set environment variables for the peer org
setGlobals() {
  local USING_ORG=""
  if [ -z "$OVERRIDE_ORG" ]; then
    USING_ORG=$1
  else
    USING_ORG="${OVERRIDE_ORG}"
  fi
  infoln "Using organization ${USING_ORG}"
  if [ $USING_ORG -eq 1 ]; then
    export CORE_PEER_LOCALMSPID="Org1MSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_ORG1_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp
    export CORE_PEER_ADDRESS=$PEER0_ORG1:$PEER0_ORG1_PORT
  elif [ $USING_ORG -eq 2 ]; then
    export CORE_PEER_LOCALMSPID="Org2MSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_ORG2_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org2.example.com/users/Admin@org2.example.com/msp
    export CORE_PEER_ADDRESS=$PEER0_ORG2:$PEER0_ORG2_PORT

  elif [ $USING_ORG -eq 3 ]; then
    export CORE_PEER_LOCALMSPID="Org3MSP"
    export CORE_PEER_TLS_ROOTCERT_FILE=$PEER0_ORG3_CA
    export CORE_PEER_MSPCONFIGPATH=${PWD}/organizations/peerOrganizations/org3.example.com/users/Admin@org3.example.com/msp
    export CORE_PEER_ADDRESS=$PEER0_ORG3:$PEER0_ORG3_PORT
  else
    errorln "ORG Unknown"
  fi

  if [ "$VERBOSE" == "true" ]; then
    env | grep CORE
  fi
}

# Set environment variables for use in the CLI container 
setGlobalsCLI() {
  setGlobals $1

  local USING_ORG=""
  if [ -z "$OVERRIDE_ORG" ]; then
    USING_ORG=$1
  else
    USING_ORG="${OVERRIDE_ORG}"
  fi
  if [ $USING_ORG -eq 1 ]; then
    export CORE_PEER_ADDRESS=$PEER0_ORG1:$PEER0_ORG1_PORT
  elif [ $USING_ORG -eq 2 ]; then
    export CORE_PEER_ADDRESS=$PEER0_ORG2:$PEER0_ORG2_PORT
  elif [ $USING_ORG -eq 3 ]; then
    export CORE_PEER_ADDRESS=$PEER0_ORG3:$PEER0_ORG3_PORT
  else
    errorln "ORG Unknown"
  fi
}

# parsePeerConnectionParameters $@
# Helper function that sets the peer connection parameters for a chaincode
# operation
parsePeerConnectionParameters() {
  PEER_CONN_PARMS=()
  PEERS=""
  while [ "$#" -gt 0 ]; do
    setGlobals $1
    PEER="peer0.org$1"
    ## Set peer addresses
    if [ -z "$PEERS" ]
    then
	PEERS="$PEER"
    else
	PEERS="$PEERS $PEER"
    fi
    PEER_CONN_PARMS=("${PEER_CONN_PARMS[@]}" --peerAddresses $CORE_PEER_ADDRESS)
    ## Set path to TLS certificate
    CA=PEER0_ORG$1_CA
    TLSINFO=(--tlsRootCertFiles "${!CA}")
    PEER_CONN_PARMS=("${PEER_CONN_PARMS[@]}" "${TLSINFO[@]}")
    # shift by one to get to the next organization
    shift
  done
}

verifyResult() {
  if [ $1 -ne 0 ]; then
    fatalln "$2"
  fi
}
