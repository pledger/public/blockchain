#!/bin/bash

eval "$(cat network-deploy.sh | head -10)"

CHANNEL_NAME="$1"
DELAY="$2"
MAX_RETRY="$3"
VERBOSE="$4"
: ${CHANNEL_NAME:="mychannel"}
: ${DELAY:="3"}
: ${MAX_RETRY:="5"}
: ${VERBOSE:="false"}

#if [ ! -d "channel-artifacts" ]; then
#	mkdir channel-artifacts
#fi

createChannelGenesisBlock() {
    which configtxgen
    if [ "$?" -ne 0 ]; then
      fatalln "configtxgen tool not found."
    fi
    infoln "Generating $CHANNEL_NAME Genesis block"
    set -x
    configtxgen -profile $CONSORTIUM_PROFILE -channelID $CHANNEL_NAME -outputBlock $CHANNEL_GENESIS_BLOCK
    kubectl cp $CHANNEL_GENESIS_BLOCK $ORDERER_NAME:/opt/gopath/src/github.com/hyperledger/fabric/ # /var/hyperledger/orderer/orderer.genesis.block
    mv $CHANNEL_GENESIS_BLOCK $DEPLOYMENT_DIR
    res=$?
    { set +x; } 2>/dev/null
    if [ $res -ne 0 ]; then
      fatalln "Failed to generate orderer genesis block..."
    fi
    successln "----------------------------------------> Genesis block created!"
}

createChannelTx() {

  which configtxgen
  if [ "$?" -ne 0 ]; then
    fatalln "configtxgen tool not found."
  fi
  set -x
  configtxgen -profile ${CHANNEL_PROFILE} -outputCreateChannelTx ${CHANNEL_NAME}.tx -channelID $CHANNEL_NAME
  res=$?
  { set +x; } 2>/dev/null
  if [ $res -ne 0 ]; then
    fatalln "Failed to generate private environment configuration transaction..."
  fi
}

createChannel() {
#	setGlobals 1
	# Poll in case the raft leader is not set yet
	PEER=$1
	local rc=1
	local COUNTER=1
	while [ $rc -ne 0 -a $COUNTER -lt $MAX_RETRY ] ; do
		sleep $DELAY
		# COMMAND in DOCKER ENV: osnadmin channel join --channelID $CHANNEL_NAME --config-block ./channel-artifacts/${CHANNEL_NAME}.block -o localhost:7053 --ca-file "$ORDERER_CA" --client-cert "$ORDERER_ADMIN_TLS_SIGN_CERT" --client-key "$ORDERER_ADMIN_TLS_PRIVATE_KEY" >&log.txt
		# ENV VARS: kubectl exec pledger-dlt-o-0 -- sh -c 'echo 'ext=$ORDERER_GENERAL_LISTENPORT' int=$ORDERER_GENERAL_LISTENPORT'
		# RESULT NOT WORKING:
		#   kubectl cp ./bin/osnadmin $ORDERER_NAME:/usr/local/bin
		#   kubectl exec $ORDERER_NAME -- osnadmin channel join --channelID $CHANNEL_NAME --config-block $CHANNEL_GENESIS_BLOCK -o $ORDERER_NAME:$ORDERER_ADMINPORT --ca-file $ORDERER_CA_DIR --client-cert $ORDERER_ADMIN_TLS_SIGN_CERT --client-key $ORDERER_ADMIN_TLS_PRIVATE_KEY >&log.txt
    kubectl cp ${CHANNEL_NAME}.tx $PEER:/opt/gopath/src/github.com/hyperledger/fabric/peer/channel-artifacts/${CHANNEL_NAME}.tx
    set -x
    kubectl exec $PEER -- peer channel create -o $ORDERER_NAME:$ORDERER_PORT -c $CHANNEL_NAME --ordererTLSHostnameOverride $ORDERER_NAME -f channel-artifacts/${CHANNEL_NAME}.tx --outputBlock channel-artifacts/${CHANNEL_NAME}.block --tls --cafile $ORDERER_CA_DIR >&log.txt
    res=$?
    { set +x; } 2>/dev/null
    kubectl cp $PEER:/opt/gopath/src/github.com/hyperledger/fabric/peer/channel-artifacts/${CHANNEL_NAME}.block ${CHANNEL_NAME}.block
    mv ${CHANNEL_NAME}.tx $DEPLOYMENT_DIR
    mv ${CHANNEL_NAME}.block $DEPLOYMENT_DIR
		let rc=$res
		COUNTER=$(expr $COUNTER + 1)
	done
	cat log.txt
	verifyResult $res "Channel creation failed"
  successln "Private environment '$CHANNEL_NAME' created"

}

# joinChannel ORG
joinChannel() {
  PEER=$1
  FABRIC_CFG_PATH=$PWD/config/
#  ORG=$1
#  setGlobals $ORG
	local rc=1
	local COUNTER=1
	## Sometimes Join takes time, hence retry
	while [ $rc -ne 0 -a $COUNTER -lt $MAX_RETRY ] ; do
    sleep $DELAY
    set -x
#    peer channel join -b $BLOCKFILE >&log.txt
    kubectl cp $DEPLOYMENT_DIR/${CHANNEL_NAME}.block $PEER:/opt/gopath/src/github.com/hyperledger/fabric/peer/channel-artifacts/${CHANNEL_NAME}.block
    kubectl exec $PEER -- peer channel join -b channel-artifacts/${CHANNEL_NAME}.block >&log.txt
    res=$?
    { set +x; } 2>/dev/null
		let rc=$res
		COUNTER=$(expr $COUNTER + 1)
	done
	cat log.txt
	verifyResult $res "After $MAX_RETRY attempts, peer0.org${ORG} has failed to join private environment '$CHANNEL_NAME' "
}

setAnchorPeer() {
  ORG=$1
  docker exec cli ./scripts/setAnchorPeer.sh $ORG $CHANNEL_NAME 
}

#FABRIC_CFG_PATH=${PWD}/configtx

## XXX Create channel genesis block
#infoln "Generating channel genesis block '${CHANNEL_NAME}.block'"
#if [ "$( kubectl get po | grep $ORDERER_NAME | awk '{print $1}' | wc -l )" -lt 1 ] ||
#   [ "$( kubectl exec $ORDERER_NAME -- sh -c "ls /var/hyperledger/orderer/*.block" | wc -l )" -lt 1 ]; then
#    cp $FABRIC_CFG_PATH/configtx.yaml $FABRIC_CFG_PATH/configtx.yaml.origin
#    envsubst < $FABRIC_CFG_PATH/configtx.yaml.origin > $FABRIC_CFG_PATH/configtx.yaml
#    createChannelGenesisBlock
#    mv $FABRIC_CFG_PATH/configtx.yaml.origin $FABRIC_CFG_PATH/configtx.yaml
#else
#    infoln "----------------------------------------> Genesis block exists."
#fi
#kubectl exec $PEER0_ORG1 -- mkdir -p /opt/gopath/src/github.com/hyperledger/fabric/peer/channel-artifacts
#kubectl exec $PEER0_ORG2 -- mkdir -p /opt/gopath/src/github.com/hyperledger/fabric/peer/channel-artifacts

infoln "Generating private environment create transaction '${CHANNEL_NAME}.tx'"
cp $FABRIC_CFG_PATH/configtx.yaml $FABRIC_CFG_PATH/configtx.yaml.origin
envsubst < $FABRIC_CFG_PATH/configtx.yaml.origin > $FABRIC_CFG_PATH/configtx.yaml
createChannelTx
mv $FABRIC_CFG_PATH/configtx.yaml.origin $FABRIC_CFG_PATH/configtx.yaml

FABRIC_CFG_PATH=$PWD/config/
#BLOCKFILE="./channel-artifacts/${CHANNEL_NAME}.block"

## Create channel

infoln "Creating private environment ${CHANNEL_NAME}"
createChannel $PEER0_ORG1

## Join all the peers to the channel
infoln "Joining $PEER0_ORG1 to the private environment..."
joinChannel $PEER0_ORG1
infoln "Joining $PEER0_ORG2 to the private environment..."
joinChannel $PEER0_ORG2

# KEEP IN MIND THAT THE CURRENT CONFIG USES THE $DLT_CONFIG_DIR OF PREV-FABRIC-ON-K8S, MAYBE SHOULD TRY WITH 2.3.2-NET VERSION
# IN ORDER TO CHECK HOW TO MAKE ORDERER-IMAGE-BUILD WORK WITH RESOLVING THE MOUNTING ISSUE OF THE GENESIS.BLOCK (WHICH IS A FILE IN PREV-, DIR IN 2.3.2-NET)

## Set the anchor peers for each org in the channel
infoln "Setting anchor peer for org1..."
#setAnchorPeer 1
./scripts/setAnchorPeer.sh $PEER0_ORG1 $PEER0_ORG1_PORT $ORG1_MSP $CHANNEL_NAME
infoln "Setting anchor peer for org2..."
#setAnchorPeer 2
./scripts/setAnchorPeer.sh $PEER0_ORG2 $PEER0_ORG2_PORT $ORG2_MSP $CHANNEL_NAME

successln "Private environment '$CHANNEL_NAME' joined"
