module github.com/LoniasGR/hyperledger-fabric-sla-chaincode/lib

go 1.18

require (
	github.com/hyperledger/fabric-gateway v1.1.1
	google.golang.org/grpc v1.50.1
)

require (
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/miekg/pkcs11 v1.1.1 // indirect
	golang.org/x/net v0.1.0 // indirect
	golang.org/x/sys v0.1.0 // indirect
	golang.org/x/text v0.4.0 // indirect
	google.golang.org/genproto v0.0.0-20221018160656-63c7b68cfc55 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)
