pipeline {
    agent any
    environment {
      ARTIFACTORY_SERVER = "https://116.203.2.204:443/artifactory/plgregistry/"
      ARTIFACTORY_DOCKER_REGISTRY = "116.203.2.204:443/plgregistry/"
      BRANCH_NAME = "master"
      VM_DEV01 = "116.203.2.205:2376"
      VM_DEV01_IP = "116.203.2.205"
      VM_DEV02 = "116.203.2.206:2376"
      VM_DEV02_IP = "116.203.2.206"
    }

    stages {
      stage('Checkout') {
          steps {
              echo 'Checkout SCM'
              checkout scm
              checkout([$class: 'GitSCM',
                        branches: [[name: env.BRANCH_NAME]],
                        extensions: [[$class: 'CleanBeforeCheckout']],
                        userRemoteConfigs: scm.userRemoteConfigs
              ])
            }
        }
        stage ('Build and Push to Artifactory') {
            steps {
              withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'Artifacts', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
                  sh 'docker image prune -f'
                  echo 'Login to Artifactory Registry'
                  sh "docker login --password=${PASSWORD} --username=${USERNAME} ${ARTIFACTORY_SERVER}"

                  echo 'Push image with Build-ID'
                  sh './scripts/build-images.sh $ARTIFACTORY_DOCKER_REGISTRY'

                  echo 'Logout from Registry'
                  sh 'docker logout $ARTIFACTORY_SERVER'
                  sh 'docker images'
              }
            }
        }

        stage ('Pull from Artifactory') {
            steps {
              withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'Artifacts', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
                  echo 'Login to Artifactory Registry'
                  sh "docker login --password=${PASSWORD} --username=${USERNAME} ${ARTIFACTORY_SERVER}"

                  echo 'Pull images from Artifactory'
                  sh './scripts/pull-images.sh $ARTIFACTORY_DOCKER_REGISTRY'

                  echo 'Logout from Registry'
                  sh 'docker logout $ARTIFACTORY_SERVER'
                  sh 'docker images'
              }
            }
        }

      stage('Remove APP_NAME from VM-DEV01') {
        steps {
          script {
            docker.withServer("$VM_DEV01", 'vm-dev01-creds') {
              sh 'export RMI=$(docker ps -a |grep $APP_NAME|awk \'{print $1;}\')'
              sh 'if [ "$RMI" > /dev/null ];then docker kill $RMI; docker rm $RMI; fi'
              sh 'docker system prune -a -f'
            }
          }
        }
      }

      stage('Deploy DLT on K8S') {
        steps {
          sh 'echo Deploying in K8S cluster'
          withKubeConfig([credentialsId: 'Jenkins_ServiceAccount' , serverUrl: 'https://192.168.70.5:6443/', namespace:'core']) {
            sh './network-deploy.sh'
          }
        } 
      }

      stage ('Clean up') {
        steps {
            deleteDir()
            sh 'ls $JENKINS_HOME/workspace/dlt*'
        }
      }

    }
}
