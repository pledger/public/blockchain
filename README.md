# Pledger Blockchain Guidelines & Documentation

## Prerequisites

### Docker 1.18+
Install Docker from: https://docs.docker.com/engine/install/ubuntu/.

### Bash 4+
Linux should be the de facto operating system when trying to run this Pledger prototype. Linux users should have a suitable version already installed on their machine, or install Bash from: https://launchpad.net/ubuntu/+source/bash.

### Kubernetes 1.19+
Install Kubernetes from: https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/.

### jq
Install the tool from here: https://stedolan.github.io/jq/download/.

### Kafka
To deploy the network, the kafka configuration is needed for the consumers. To create it:

1. Install `openjdk-8-jre-headless` or similar (`keytool` is needed) and `openssl`.
2. Run `scripts/JKS2PEM.sh`.
   For example run:
   ```bash
    ./scripts/JKS2PEM.sh ./kafka-config/kafka.client.truststore.jks ./kafka-config/server.cer.pem
   ```
3. Copy all kafka configuration files to `config/kafka`.

## DLT on Kubernetes

**DLT** is an orchestrated blockchain framework. In order to follow the Pledger orchestration and container-based mentality, the prototype is adopting an orchestration nature for the network and each of the hosted components. The deployment of **trusted nodes** and **DApps** inside the **DLT** are respectively modified and re-organized in order to follow the orchestration character of the prototype.

| Phase | Description |
|------|------------|
| Migration | The necessary artifacts of the **DLT** are modified in order to adapt to the YAMLs specifications (https://yaml.org/spec/). |
| Integration | The blockchain framework is pre-configured in order to be deployable on a single cluster.|
| Deployment | Every **DLT** component is installed and deployed on Kubernetes.|

### Set DLT network up
```
./network-k8s.sh up
```

## SLASC Bridge

### Description

The main scenario of the **SLASC Bridge** workflow and interactions starts from the configuration of an SLA in the _ConfService_ through the _SLA Lite_. Afterwards, the representation of the _SLA contractual terms_ arises through the **SLASC Bridge** which resolves the deployment of the equivalent _smart contract structures_ on the **DLT** network. In each _SLA configuration_ that is completed by the _ConfService_, a respective notification is sent to the **SLASC Bridge**. The configured SLA parameters that originate from the _ConfService_ and are included in the notifications sent between the two components, are then consumed by the **SLASC Bridge** that initiates the process of the smart contract structure creation. The **SLASC Bridge** defines a new structure that constitutes the smart contract equivalent of the received SLA. The smart contract equivalent is to be submitted on the ledger in the final step. During the **SLASC Bridge** operation, smart contract equivalents of SLA configurations are formed and ready to be submitted on the ledger as a decentralized representation of the _SLA parametrization_. Additionally, the received SLAs are automatically included in the refunding mechanism that is deployed on the **DLT**, and will be described in the next paragraph. In the final step, the equivalent smart contract structures are submitted on the blockchain network.

![SLASC Bridge Interactions](slasc-interactions.png "SLASC Bridge Interactions")


### Deploy
```
./network-k8s.sh channel create sla 1
./network-k8s.sh chaincode deploy 1 slasc-bridge ./ccas_sla
```

### Set Network Down 
```  
docker kill kind-registry || true
docker rm kind-registry || true
```

### Screencast
1. [Set network down](#set-network-down)

2. Start screencast

3. [Set DLT network up](#set-dlt-network-up)

4. [Deploy SLASC Bridge](#deploy)

## Legal
**DLT** and its features are released under the [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0) license ([LICENSE](https://gitlab.com/pledger/public/blockchain/-/blob/main/LICENSE)).

Copyright © 2019-2022 Innov-Acts Ltd. All rights reserved.

| ![EU Flag](http://www.consilium.europa.eu/images/img_flag-eu.gif) | This work has received funding by the European Commission under grant agreement No. 871536, Pledger project. |
|---|--------------------------------------------------------------------------------------------------------|
